import {RadarChart} from "./XYFader.js"
import * as d3 from "https://cdn.skypack.dev/d3@5"

var margin = { top: 100, right: 100, bottom: 100, left: 100 },
width = Math.min(700, window.innerWidth - 10) - margin.left - margin.right,
height = Math.min(
    width,
    window.innerHeight - margin.top - margin.bottom - 20
);

var data = [
  [
    { axis: "Argentino Rioplatense", value: 0 },
    { axis: "Colombiano", value: 0 },
    { axis: "Cubano", value: 0 },
    // { axis: "Peruano", value: 0 },
    // { axis: "Boliviano", value: 0 },
  ]
];

var color = d3.scaleOrdinal().range(["#EDC951", "#CC333F", "#00A0B0"]);

var radarChartOptions = {
  container : "#mainContainer",
  w: width,
  h: height,
  margin: margin,
  maxValue: 0.5,
  levels: 5,
  roundStrokes: true,
  color: color,
  onChange : function(p) {
    // console.log(p)
  }
};

RadarChart(data, radarChartOptions);